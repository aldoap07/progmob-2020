package ukdw.com.progmob_2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import retrofit2.http.Field;

public class Dosen {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("namaDos")
    @Expose
    private String namaDos;

    @SerializedName("nidn")
    @Expose
    private String nidn;

    @SerializedName("gelar")
    @Expose
    private String gelar;

    @SerializedName("alamatDos")
    @Expose
    private String alamatDos;

    @SerializedName("emailDos")
    @Expose
    private String emailDos;

    @SerializedName("fotoDos")
    @Expose
    private String fotoDos;

    public Dosen(String namaDos, String nidn, String gelar, String alamatDos) {
        this.namaDos = namaDos;
        this.nidn = nidn;
        this.gelar = gelar;
        this.alamatDos = alamatDos;
    }

    public Dosen(String id, String namaDos, String nidn, String gelar, String alamatDos, String emailDos, String fotoDos ) {
        this.id = id;
        this.namaDos = namaDos;
        this.nidn = nidn;
        this.gelar = gelar;
        this.alamatDos = alamatDos;
        this.emailDos = emailDos;
        this.fotoDos = fotoDos;
    }

    public Dosen(String namaDos, String nidn, String gelar, String alamatDos, String emailDos, String fotoDos) {
        this.namaDos = namaDos;
        this.nidn = nidn;
        this.gelar = gelar;
        this.alamatDos = alamatDos;
        this.emailDos = emailDos;
        this.fotoDos = fotoDos;
    }

    public String getNamaDos() {
        return namaDos;
    }

    public void setNamaDos(String nama) {
        this.namaDos = namaDos;
    }

    public String getNidn() {
        return nidn;
    }

    public void setNidn(String nidn) {
        this.nidn = nidn;
    }

    public String getGelar() {
        return gelar;
    }

    public void setGelar(String gelar) {
        this.gelar = gelar;
    }

    public String getAlamatDos() {
        return alamatDos;
    }

    public void setAlamatDos(String alamatDos) {
        this.alamatDos = alamatDos;
    }

    public String getEmailDos() {
        return emailDos;
    }

    public void setEmailDos(String emailDos) {
        this.emailDos = emailDos;
    }

    public String getFotoDos() {
        return fotoDos;
    }

    public void setFotoDos(String fotoDos) {
        this.fotoDos = fotoDos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
