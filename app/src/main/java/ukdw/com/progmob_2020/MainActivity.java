package ukdw.com.progmob_2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ukdw.com.progmob_2020.Pertemuan3.ListActivity;
import ukdw.com.progmob_2020.Pertemuan3.RecyclerActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        Button btnText = (Button)findViewById(R.id.button1);
        final EditText myEditText = (EditText)findViewById(R.id.editText1);
        Button btnHelp = (Button)findViewById((R.id.btnHelp));
        Button btnTracker = (Button)findViewById((R.id.btnTracker));

        Button btnList = (Button)findViewById(R.id.btnList);
        Button btnRecycle = (Button)findViewById(R.id.btnRecycler);
        Button btnCard = (Button)findViewById(R.id.btnCard);

        txtView.setText(R.string.konstanta);
        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d("COBA KLIKKK KRUB",myEditText.getText().toString());
                txtView.setText(myEditText.getText().toString());
            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,HelpActivity.class);
                Bundle b = new Bundle();

                b.putString("helpString",myEditText.getText().toString());
                intent.putExtras(b);

                startActivity(intent);
            }
        });
        btnTracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainActivity.this,TrackerActivity.class);

                startActivity(intn);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iintn = new Intent(MainActivity.this, ListActivity.class);
                startActivity(iintn);
            }
        });

        btnRecycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iintn = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(iintn);
            }
        });

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iintn = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(iintn);
            }
        });
    }
}