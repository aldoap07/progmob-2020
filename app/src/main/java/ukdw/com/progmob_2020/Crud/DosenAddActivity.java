package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText edNamaDos = (EditText)findViewById(R.id.edNamaDos);
        final EditText edNidnDos = (EditText)findViewById(R.id.edNidnDos);
        final EditText edGelarDos = (EditText)findViewById(R.id.edGelarDos);
        final  EditText edAlamatDos = (EditText)findViewById(R.id.edAlamatDos);
        Button btnSvDos = (Button)findViewById(R.id.btnSvDos);
        pd = new ProgressDialog( DosenAddActivity.this);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.add_dsn(
                edNamaDos.getText().toString(),
                edNidnDos.getText().toString(),
                edGelarDos.getText().toString(),
                "argo.wibowo@si.ukdw.ac.id",
                "Argo Wibowo",
                "S.Kom"
        );

        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                Toast.makeText(DosenAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DosenAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
            }
        });
    }
}