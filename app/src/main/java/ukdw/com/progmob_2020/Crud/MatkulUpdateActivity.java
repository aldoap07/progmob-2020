package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        final EditText edUpMat = (EditText)findViewById(R.id.edUpMat);
        final EditText edUpKode = (EditText)findViewById(R.id.edUpKode);
        final EditText edUpHari = (EditText)findViewById(R.id.edUpHari);
        final EditText edUpSesi = (EditText)findViewById(R.id.edUpSesi);
        final EditText edUpSks = (EditText)findViewById(R.id.edUpSks);
        Button btnUpMat = (Button)findViewById(R.id.btnUpMat);
        pd = new ProgressDialog( MatkulUpdateActivity.this);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.update_mat(
                edUpMat.getText().toString(),
                edUpKode.getText().toString(),
                edUpHari.getText().toString(),
                edUpSesi.getText().toString(),
                edUpSks.getText().toString(),
                "72180179"
        );

        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                Toast.makeText(MatkulUpdateActivity.this, "DATA BERHASIL DIUPDATE", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MatkulUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

            }
        });
    }
}