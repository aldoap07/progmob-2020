package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class MainDsnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dsn);

        Button btnAddDos = (Button)findViewById(R.id.btnAddDos);
        Button btnHpsDos = (Button)findViewById(R.id.btnHpsDos);
        Button btnUpdateDos = (Button)findViewById(R.id.btnUpdateDos);
        Button btnGetDos = (Button)findViewById(R.id.btnGetDos);

        btnAddDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainDsnActivity.this, DosenAddActivity.class);

                startActivity(intn);
            }
        });

        btnUpdateDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainDsnActivity.this, DosenUpdateActivity.class);

                startActivity(intn);
            }
        });


    }
}