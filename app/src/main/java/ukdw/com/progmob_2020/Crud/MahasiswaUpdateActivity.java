package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        final EditText edNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        final EditText edNimBaru = (EditText)findViewById(R.id.editTextNimBaru);
        final EditText edAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaru);
        final EditText edEmailBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdate);
        pd = new ProgressDialog( MahasiswaUpdateActivity.this);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.update_mhs(
                edNamaBaru.getText().toString(),
                edNimBaru.getText().toString(),
                edAlamatBaru.getText().toString(),
                "aldo.anggi@si.ukdw.ac.id",
                "Aldo Anggi Prayogi",
                "72180179"
        );
        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                Toast.makeText(MahasiswaUpdateActivity.this, "DATA BERHASIL DIUPDATE", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MahasiswaUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

            }
        });
    }
}