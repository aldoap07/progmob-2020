package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
public class MatkulAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);

        final EditText edAddMat = (EditText)findViewById(R.id.edAddMat);
        final EditText edAddKode = (EditText)findViewById(R.id.edAddKode);
        final EditText edAddHari = (EditText)findViewById(R.id.edAddHari);
        final EditText edAddSesi = (EditText)findViewById(R.id.edAddSesi);
        final EditText edAddSks = (EditText)findViewById(R.id.edAddSks);
        Button btnAddMat = (Button)findViewById(R.id.btnAddMat);
        pd = new ProgressDialog( MatkulAddActivity.this);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.add_mat(
                edAddMat.getText().toString(),
                edAddKode.getText().toString(),
                edAddHari.getText().toString(),
                edAddSesi.getText().toString(),
                edAddSks.getText().toString(),
                "72180179"
        );

        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                Toast.makeText(MatkulAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MatkulAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

            }
        });
    }
}