package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahasiswaDeleteActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_delete);

        final EditText edHapusNim = (EditText)findViewById(R.id.editTextNimHapus);
        Button btnHapusNim = (Button)findViewById(R.id.btnDelete);
        pd = new ProgressDialog( MahasiswaDeleteActivity.this);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.delete_mhs(
                edHapusNim.getText().toString(),
                "72180179"
        );

        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                Toast.makeText(MahasiswaDeleteActivity.this, "DATA BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MahasiswaDeleteActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

            }
        });
    }
}