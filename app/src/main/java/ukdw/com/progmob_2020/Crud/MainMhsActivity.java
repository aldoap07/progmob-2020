package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import ukdw.com.progmob_2020.R;


public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button btnAddMhs = (Button)findViewById(R.id.btnAddMhs);
        Button btnUpMhs = (Button)findViewById(R.id.btnUpMhs);
        Button btnGetMhs = (Button)findViewById(R.id.btnGetMhs);
        Button btnDelMhs = (Button)findViewById(R.id.btnDelMhs);

        btnAddMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMhsActivity.this, MahasiswaAddActivity.class);

                startActivity(intn);
            }
        });

        btnDelMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMhsActivity.this, MahasiswaDeleteActivity.class);

                startActivity(intn);
            }
        });

        btnUpMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMhsActivity.this, MahasiswaUpdateActivity.class);

                startActivity(intn);
            }
        });

        btnGetMhs.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMhsActivity.this, MahasiswaGetActivity.class);

                startActivity(intn);
            }
        }));
    }
}