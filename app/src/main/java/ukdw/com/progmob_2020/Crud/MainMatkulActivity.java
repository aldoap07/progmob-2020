package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ukdw.com.progmob_2020.R;

public class MainMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        Button btnAddMat = (Button)findViewById(R.id.btnAddMat);
        Button btnUpMat = (Button)findViewById(R.id.btnUpMat);
        Button btnGetMat = (Button)findViewById(R.id.btnGetMat);
        Button btnDeMat = (Button)findViewById(R.id.btnDeleteMat);

        btnAddMat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMatkulActivity.this, MatkulAddActivity.class);

                startActivity(intn);
            }
        });

        btnUpMat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMatkulActivity.this, MatkulUpdateActivity.class);

                startActivity(intn);
            }
        });

        btnDeMat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent (MainMatkulActivity.this, MatkulDeleteActivity.class);

                startActivity(intn);
            }
        });
    }
}