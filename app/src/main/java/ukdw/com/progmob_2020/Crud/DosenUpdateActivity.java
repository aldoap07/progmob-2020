package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        final EditText edNmDosBaru = (EditText)findViewById(R.id.edNmDosBaru);
        final EditText edNidnDosBaru = (EditText)findViewById(R.id.edNidnDosBaru);
        final EditText edGelarDosBaru = (EditText)findViewById(R.id.edGelarDosBaru);
        final EditText edEmailDosBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        final EditText edAlamatDosBaru = (EditText)findViewById(R.id.edAlamatDosBaru);
        pd = new ProgressDialog( DosenUpdateActivity.this);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.update_dsn(
                edNmDosBaru.getText().toString(),
                edNidnDosBaru.getText().toString(),
                edAlamatDosBaru.getText().toString(),
                "aldo.anggi@si.ukdw.ac.id",
                "Aldo Anggi Prayogi",
                "72180179"
        );
        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                Toast.makeText(DosenUpdateActivity.this, "DATA BERHASIL DIUPDATE", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DosenUpdateActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

            }
        });
    }
}