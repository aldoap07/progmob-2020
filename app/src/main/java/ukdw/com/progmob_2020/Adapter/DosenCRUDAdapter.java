package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.R;


public class DosenCRUDAdapter extends RecyclerView.Adapter<DosenCRUDAdapter.ViewHolder> {
    private Context context;

    private List<Dosen> dosenList;

    public DosenCRUDAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_dosen,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNamaDos.setText(d.getNamaDos());
        holder.tvNidnDos.setText(d.getNidn());
        holder.tvGelarDos.setText(d.getGelar());
        holder.tvAlamatDos.setText(d.getAlamatDos());
    }


    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaDos, tvNidnDos, tvGelarDos, tvAlamatDos;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaDos = itemView.findViewById((R.id.tvNamaDos));
            tvNidnDos = itemView.findViewById((R.id.tvNidnDos));
            tvGelarDos = itemView.findViewById((R.id.tvGelarDos));
            tvAlamatDos = itemView.findViewById(R.id.tvAlamatDos);
        }
    }
}
